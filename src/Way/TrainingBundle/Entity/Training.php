<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 07.06.15
 * Time: 14:43
 */

namespace Way\TrainingBundle\Entity;

use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping as ORM;


/**
 * Training
 *
 * @ORM\Table(name="Training")
 * @ORM\Entity()
 */
class Training {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="training_token", type="string", length=255, nullable=false)
     */
    private $trainingToken;

    /**
     * @OneToOne(targetEntity="TrainingResume", mappedBy="training")
     **/
    private $trainingResume;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */

    private $created;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Way\TrainingBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10, nullable=false)
     */
    private $status;



    public function __construct() {
        $this->created = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTrainingToken()
    {
        return $this->trainingToken;
    }

    /**
     * @param string $trainingToken
     */
    public function setTrainingToken($trainingToken)
    {
        $this->trainingToken = $trainingToken;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return TrainingResume
     */
    public function getTrainingResume()
    {
        return $this->trainingResume;
    }

    /**
     * @param TrainingResume $trainingResume
     */
    public function setTrainingResume($trainingResume)
    {
        $this->trainingResume = $trainingResume;
    }


}