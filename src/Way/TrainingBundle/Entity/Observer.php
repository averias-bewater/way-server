<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 08.08.15
 * Time: 16:07
 */

namespace Way\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="Observer")
 * @ORM\Entity()
 */
class Observer {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="observers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=FALSE)
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="observerOf")
     * @ORM\JoinColumn(name="observer_id", referencedColumnName="id", nullable=FALSE)
     */
    private $observer;

    /**
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     *
     */
    private $created;


    public function __construct(){
        $this->created = new \DateTime();
        $this->status = "pending";
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObserver()
    {
        return $this->observer;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setObserver(User $user = null)
    {
        $this->observer = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }


}