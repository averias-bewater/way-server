<?php
/**
 * @project    way-server
 * @author     Rafael Campoy <rafa.campoy@gmail.com>
 */

namespace Way\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * AuthToken
 *
 * @ORM\Table(name="AuthToken")
 * @ORM\Entity
 *
 * @ExclusionPolicy("none")
 */
class AuthToken
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="token", type="string", length=255, nullable=false)
     */
    private $token;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Way\TrainingBundle\Entity\User", inversedBy="authTokens")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     *
     * @Exclude()
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire", type="datetime", nullable=false)
     */
    private $expire;

    public function __construct() {
        $this->created = new \DateTime();
    }

    /**
     * @param \Way\TrainingBundle\Entity\User $user
     */
    public function setUser(User $user) {
        $this->user = $user;
    }

    /**
     * @return \Way\TrainingBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return AuthToken
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set expire
     *
     * @param \DateTime $expire
     * @return AuthToken
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;

        return $this;
    }

    /**
     * Get expire
     *
     * @return \DateTime
     */
    public function getExpire()
    {
        return $this->expire;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

}
