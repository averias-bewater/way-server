<?php
/**
 * @project    way-server
 * @author     Rafael Campoy <rafa.campoy@gmail.com>
 */

namespace Way\TrainingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(
 *  name="User",
 *  uniqueConstraints={
 *      @ORM\UniqueConstraint(name="username_unique", columns={"username"})
 *  }
 * )
 * @ORM\Entity()
 */
class User implements UserInterface, \Serializable, \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="register_date", type="datetime", nullable=false)
     */
    private $registerDate;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Way\TrainingBundle\Entity\AuthToken", mappedBy="user")
     */
    private $authTokens;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=false)
     */
    private $surname;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=false)
     */
    private $height;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", nullable=false)
     */
    private $weight;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime", nullable=false)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=1, nullable=false)
     */
    private $gender;

    /**
     * @ORM\OneToMany(targetEntity="Observer", mappedBy="user", cascade={"remove"})
     */
    protected $observers;

    /**
     * @ORM\OneToMany(targetEntity="Observer", mappedBy="observer", cascade={"remove"})
     */
    protected $observerOf;

    public function __construct() {
        $this->registerDate = new \DateTime();
        $this->observers = new ArrayCollection();
        $this->observerOf = new ArrayCollection();
        $this->authTokens = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Required Method by UserInterface
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Required Method by UserInterface
     */
    public function eraseCredentials() {}

    /**
     * Required Method by UserInterface
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     */
    public function setRegisterDate(\DateTime $registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @param AuthToken $authToken
     */
    public function addAuthToken(AuthToken $authToken) {
        $this->authTokens->add($authToken);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAuthTokens() {
        return $this->authTokens;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }



    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize($this->id);
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        $this->id = unserialize($serialized);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getObservers()
    {
        return $this->observers;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getActiveObservers()
    {
        $activeObservers = clone $this->observers;
        /**
         * @var Observer $observer
         */
        foreach ($activeObservers as $key => $observer) {
            if('active' !== $observer->getStatus()) {
                unset($activeObservers[$key]);
            }
        }

        return $activeObservers;
    }

    /**
     * @param mixed $observers
     */
    public function setObservers($observers)
    {
        $this->observers = $observers;
    }

    /**
     * Add Observer
     *
     * @param \Way\TrainingBundle\Entity\Observer $observer
     * @return User
     */
    public function addObservers(Observer $observer)
    {
        if (!$this->observers->contains($observer)) {
            $this->observers->add($observer);
            $observer->setUser($this);
        }

        return $this;
    }

    /**
     * Remove Observers
     *
     * @param \Way\TrainingBundle\Entity\Observer $observer
     * @return User
     */
    public function removeObservers(Observer $observer)
    {
        if ($this->observers->contains($observer)) {
            $this->observers->removeElement($observer);
            $observer->setUser(null);
        }

        return $this;
    }

    /**
     * Check if observer is associate to the user
     *
     * @param $id
     *
     * @return bool
     */
    public function hasObservers($id) {
        foreach($this->getObservers() as $observer) {
            /** @var Observer $observer */
            if($observer->getObserver()->getId() === $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getObserverOf()
    {
        return $this->observerOf;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getActiveObserverOf()
    {
        $activeObserverOf = clone $this->observerOf;
        /**
         * @var Observer $observer
         */
        foreach ($activeObserverOf as $key => $observerOf) {
            if('active' !== $observerOf->getStatus()) {
                unset($activeObserverOf[$key]);
            }
        }

        return $activeObserverOf;
    }

    /**
     * @param mixed $users
     */
    public function setObserverOf($users)
    {
        $this->observerOf = $users;
    }

    /**
     * Add ObserverOf
     *
     * @param \Way\TrainingBundle\Entity\Observer $observer
     * @return User
     */
    public function addObserverOf(Observer $observer)
    {
        if (!$this->observerOf->contains($observer)) {
            $this->observerOf->add($observer);
            $observer->setObserver($this);
        }

        return $this;
    }

    /**
     * Remove ObserverOf
     *
     * @param \Way\TrainingBundle\Entity\Observer $observer
     * @return User
     */
    public function removeObserverOf(Observer $observer)
    {
        if ($this->observerOf->contains($observer)) {
            $this->observerOf->removeElement($observer);
            $observer->setObserver(null);
        }

        return $this;
    }

    /**
     * Check if user is observer of
     *
     * @param $id
     *
     * @return bool
     */
    public function isObserverOf($id) {
        foreach($this->getObserverOf() as $observerOf) {
            /** @var Observer $observerOf */
            if($observerOf->getUser()->getId() === $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Custom method to serialize this entity to JSON.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'username' => $this->getUsername(),
            'registerDate' => $this->registerDate->format('c')
        );
    }
}
