<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 29.06.15
 * Time: 14:05
 */

namespace Way\TrainingBundle\Entity;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping as ORM;

/**
 * Training
 *
 * @ORM\Table(name="TrainingResume")
 * @ORM\Entity()
 */
class TrainingResume {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @OneToOne(targetEntity="Training", inversedBy="trainingResume")
     * @JoinColumn(name="training_id", referencedColumnName="id")
     **/
    private $training;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="decimal", precision=20, scale=0, nullable=false)
     */
    private $duration;

    /**
     * @var float
     *
     * @ORM\Column(name="distance", type="float", nullable=false)
     */
    private $distance;

    /**
     * @var float
     *
     * @ORM\Column(name="speed", type="float", nullable=false)
     */
    private $speed;


    /**
     * @var string
     *
     * @ORM\Column(name="calories", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $calories;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Training
     */
    public function getTraining()
    {
        return $this->training;
    }

    /**
     * @param Training $training
     */
    public function setTraining($training)
    {
        $this->training = $training;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return float
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param float $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param float $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return string
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * @param string $calories
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;
    }

    /**
     * Custom method to serialize this entity to JSON.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'training_id' => $this->getTraining()->getId(),
            'name' => $this->getName(),
            'distance' => $this->getDistance(),
            'duration' => $this->getDuration(),
            'speed' => $this->getSpeed(),
            'calories' => $this->getCalories()
        );
    }
}