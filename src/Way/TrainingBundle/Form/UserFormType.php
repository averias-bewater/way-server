<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 06.06.15
 * Time: 03:00
 */

namespace Way\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password')
            ->add('salt')
            ->add('name')
            ->add('surname')
            ->add('height', 'integer')
            ->add('weight', 'number')
            ->add('birth_date', 'datetime' ,array(
                'widget'=> 'single_text',
                'input' => 'datetime',
                'date_format'=>'yyyy-MM-dd HH:mm:ss',
                'invalid_message' => 'Error in birth date'
            ))
            ->add('gender')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'Way\TrainingBundle\Entity\User',
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }
}

