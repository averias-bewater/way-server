<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 07.06.15
 * Time: 14:43
 */

namespace Way\TrainingBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use Way\TrainingBundle\Entity\Training;
use Way\TrainingBundle\Entity\TrainingResume;
use Way\TrainingBundle\Entity\User;

class TrainingController extends FOSRestController
{
    /**
     * Start a new training session creating a new Training with status = 'started' and generating and sending
     * a new training token
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong.",
     *          403="Returned when user/token doesn't exist or match"
     *      }
     * )
     *
     * @Post("/start")
     * @RequestParam(name="token", description="authorization token", strict=true)
     * @RequestParam(name="user_id", description="user id", strict=true)
     *
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @return JsonResponse
     */
    public function startAction(ParamFetcher $paramFetcher)
    {
        $token = $paramFetcher->get('token');
        $userId = $paramFetcher->get('user_id');

        // check if data are correct
        if (null === $token || null === $userId) {
            throw new BadRequestHttpException('no token or user id sent');
        }

        $authToken = $this->get('user.auth_token_service')->checkToken($token, $userId);

        $tokenGenerator =  new UriSafeTokenGenerator();
        $trainingToken = $tokenGenerator->generateToken();

        $training = new Training();
        $training->setUser($authToken->getUser());
        $training->setTrainingToken($trainingToken);
        $training->setStatus('started');

        $em = $this->getDoctrine()->getManager();

        $em->persist($training);
        $em->flush();

        $observers = [];
        $user = $authToken->getUser();
        foreach ($this->get('observers')->getObservers($user->getId()) as $observer) {
            $observers[] = $observer['id'];
        }

        return new JsonResponse(
            [
                'training_token' => $trainingToken,
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'observers' => $observers
            ]
        );
    }

    /**
     * Mark a training as ended, that is, setting training status = 'ended'
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong."
     *      }
     * )
     *
     * @Post("/end")
     * @RequestParam(name="training_token", description="authorization token", strict=true)
     * @RequestParam(name="user_id", description="user id", strict=true)
     *
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @return JsonResponse
     */
    public function endAction(ParamFetcher $paramFetcher)
    {
        $token = $paramFetcher->get('training_token');
        $userId = $paramFetcher->get('user_id');

        // check if data are correct
        if (null === $token || null === $userId) {
            throw new BadRequestHttpException('no token or user id sent');
        }

        $em = $this->getDoctrine()->getManager();

        /** @var Training $trainingSession */
        $trainingSession = $em->getRepository('WayTrainingBundle:Training')->findOneBy(
            [
                'user' => $userId,
                'trainingToken' => $token,
                'status' => 'started'
            ]
        );

        if (null ===$trainingSession) {
            throw new BadRequestHttpException('no started training session');
        }


        $trainingSession->setStatus('ended');

        $em->persist($trainingSession);
        $em->flush();

        return new JsonResponse(['status' => 'ended'], 200);
    }

    /**
     * Depending on 'saved' request parameter value, training cannot be saved and in this case coordinates will
     * be deleted and training status = 'unsaved'. If training if saved, training status = 'saved', coordinates
     * are sync and resume data are recalculated
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          204="Returned when successful.",
     *          401="Returned when the user is not authorized."
     *      }
     * )
     *
     * @Post("/save")
     * @RequestParam(array=true, name="data", description="training data", strict=true)
     *
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function saveTrainigAction(ParamFetcher $paramFetcher)
    {
        // check request data
        $data = $paramFetcher->all()['data'];
        $checkedRequest = $this->checkSaveTrainigRequest($data);
        if (true !== $checkedRequest) {
            throw new BadRequestHttpException($checkedRequest[1]);
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository('WayTrainingBundle:User')->find($data['user_id']);
        if (null === $user) {
            throw new BadRequestHttpException('user not found');
        }

        /** @var Training $trainingSession */
        $trainingSession = $em->getRepository('WayTrainingBundle:Training')->findOneBy(
            [
                'user' => $user,
                'trainingToken' => $data['training_token']
            ]
        );

        if (null === $trainingSession) {
            throw new BadRequestHttpException('no started training session, user/token combination not found');
        }

        $elasticSearchService = $this->get('training.elastic_search_service');

        $count = 0;

        if (false === $data['save']) {
            $trainingSession->setStatus('unsaved');
            $count = $elasticSearchService->deleteTrainingData($data['user_id'], $data['training_token']);
        } else {
            $trainingSession->setStatus('saved');

            $count = $elasticSearchService->syncTrainingData($data['user_id'], $data['training_token'], $data['coordinates']);

            $trainingResume = new TrainingResume();
            $trainingResume->setTraining($trainingSession);
            $trainingResume->setName($data['resume']['training_name']);
            $trainingResume->setDuration($data['resume']['duration']);
            $trainingResume->setDistance($data['resume']['distance']);
            $trainingResume->setSpeed($data['resume']['speed']);
            $trainingResume->setCalories($data['resume']['calories']);

            $em->persist($trainingResume);
        }

        $em->persist($trainingSession);
        $em->flush();

        return new JsonResponse(['saved' => $data['save'], 'count' => $count], 200);
    }

    /**
     * return observers for one user and check if user if authenticated
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong.",
     *          403="Returned when user/token doesn't exist or match"
     *      }
     * )
     *
     * @Post("/observers")
     * @RequestParam(name="token", description="authorization token", strict=true)
     * @RequestParam(name="user_id", description="user id", strict=true)
     *
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @return JsonResponse
     */
    public function getActiveObserversAction(ParamFetcher $paramFetcher)
    {
        $token = $paramFetcher->get('token');
        $userId = $paramFetcher->get('user_id');

        // check if data are correct
        if (null === $token || null === $userId) {
            throw new BadRequestHttpException('no token or user id sent');
        }

        $authToken = $this->get('user.auth_token_service')->checkToken($token, $userId);

        $observers = [];
        $user = $authToken->getUser();
        foreach ($this->get('observers')->getObservers($user->getId()) as $observer) {
            $observers[] = $observer['id'];
        }

        return new JsonResponse(
            [
                'observers' => $observers,
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
            ]
        );
    }

    /**
     * @param array $requestData
     * @return array|bool
     */
    private function checkSaveTrainigRequest(array $requestData) {
        if (true === empty($requestData)) {
            return [false, 'request data are empty'];
        }

        if (false === isset($requestData['training_token'])) {
            return [false, 'request token value  does not exist'];
        }

        if (false === isset($requestData['user_id'])) {
            return [false, 'request user value does not exist'];
        }

        if (false === isset($requestData['save'])) {
            return [false, 'request save value  does not exist'];
        }

        if (false === isset($requestData['coordinates'])) {
            return [false, 'request coordinates value  does not exist'];
        }

        if (false === isset($requestData['resume'])) {
            return [false, 'request resume value  does not exist'];
        }

        return true;
    }
}
