<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 02.06.15
 * Time: 03:48
 */

namespace Way\TrainingBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller {

    /**
     * Exists only for the route-definition, symfony manages login
     * see /app/config/security.yml
     *
     * @Route("/login")
     * @Method("POST")
     */
    public function loginAction(Request $request) {}

    /**
     * Exists only for the route-definition, symfony manages logout
     * see /app/config/security.yml
     *
     * @Route("/logout")
     * @Method("GET|POST")
     */
    public function logoutAction() {}

}