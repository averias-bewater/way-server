<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 06.06.15
 * Time: 02:35
 */

namespace Way\TrainingBundle\Controller;


use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Way\TrainingBundle\Entity\AuthToken;
use Way\TrainingBundle\Entity\Training;
use Way\TrainingBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Way\TrainingBundle\Form\UserFormType;

class UserController extends FOSRestController {

    /**
     * Save a new user
     *
     * @ApiDoc(
     *      section="UserManagement",
     *      statusCodes={
     *          204="Returned when successful.",
     *          401="Returned when the user is not authorized."
     *      }
     * )
     *
     * @Post("/user/save")
     * @RequestParam(array=true, name="data", description="new user data", strict=true)
     *
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function saveAction(ParamFetcher $paramFetcher){

        $userData = $paramFetcher->all()['data'];

        $logger = $this->get("logger");
        $logger->info("user data received", $userData);
        if(empty($userData)){
            $logger->info('No data received');
            throw new BadRequestHttpException('No data received');
        }

        // check if user already exists
        $username = $userData['username'];
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('WayTrainingBundle:User')->findOneBy(['username' => $username]);

        if (null !== $user) {
            $logger->info('user already exists');
            return new JsonResponse(
                [
                    "code" => 0, // user already exists,
                    "message" => 'user already exists'
                ],
                200
            );
        }

        // create new user
        $user = new User();
        $form = $this->createForm(new UserFormType(), $user);
        $form->submit($userData);

        if (!$form->isValid()) {
            $logger->info('Invalid data received');
            throw new BadRequestHttpException('Invalid data received');
        }


        $salt = md5(time());
        $password = $this->get('user.security.password_encoder')->encodePassword($user->getPassword(), $salt);
        $user->setSalt($salt);
        $user->setPassword($password);

        $em->persist($user);
        $em->flush();

        // generate user token
        /** @var AuthToken $userToken */
        $userToken = $this->get('user.auth_token_service')->save($user);

        return new JsonResponse(
            [
//                'token' => $userToken->getToken(),
//                'expire' => $userToken->getExpire()->format('c'),
                "user_id" => $userToken->getUser()->getId(),
                "code" => 1, // user created,
                "message" => 'user created'
            ],
            201
        );
    }

    /**
     * Edit a new user
     *
     * @ApiDoc(
     *      section="UserManagement",
     *      statusCodes={
     *          204="Returned when successful.",
     *          401="Returned when the user is not authorized."
     *      }
     * )
     *
     * @Put("/user/edit/{id}", requirements = {"id" = "\d+"})
     * @RequestParam(array=true, name="data", description="new user data", strict=true)
     *
     * @param mixed $id
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function updateAction($id, ParamFetcher $paramFetcher){

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository('WayTrainingBundle:User')->find($id);

        if (null === $user) {
            throw new BadRequestHttpException('user does not exist');
        }

        $userData = $paramFetcher->all()['data'];

        if(empty($userData)){
            throw new BadRequestHttpException('No data received');
        }

        $form = $this->createForm(new UserFormType(), $user);
        $form->submit($userData);

        if (!$form->isValid()) {
            throw new BadRequestHttpException('Invalid data received');
        }

        $salt = md5(time());
        $password = $this->get('user.security.password_encoder')->encodePassword($user->getPassword(), $salt);
        $user->setSalt($salt);
        $user->setPassword($password);
        $em->persist($user);
        $em->flush();

        // generate new user token
        /** @var AuthToken $userToken */
        $userToken = $this->get('user.auth_token_service')->save($user);

        return new JsonResponse(array(
            'token' => $userToken->getToken(),
            'expire' => $userToken->getExpire()->format('c')
        ));

    }

    /**
     * Return all training for a user with training status='saved'
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong."
     *      }
     * )
     *
     * @Get("/user/{id}/trainings")
     *
     * @param int $id
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @return JsonResponse
     */
    public function getAllTrainingByUserAction($id)
    {

        if (empty($id)) {
            throw new BadRequestHttpException('bad user id received');
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository('WayTrainingBundle:User')->find($id);
        if (null === $user) {
            throw new BadRequestHttpException('user not found');
        }


        /** @var User $authUser */
        $authUser = $this->get('security.token_storage')->getToken()->getUser();
        if ($authUser->getId() !== $user->getId()) {
            throw new AccessDeniedHttpException('access denied to see training for user ' . $id);
        }

        /** @var QueryBuilder $qb */
        $qb = $em->createQueryBuilder();
        $qb->select(
            't.id as training_id',
            't.trainingToken as training_token',
            'tr.name',
            'tr.duration',
            'tr.distance'
        )
            ->from('WayTrainingBundle:Training', 't')
            ->innerJoin('t.trainingResume', 'tr')
            ->where('t.user = ?1')
            ->andWhere('t.status = \'saved\'')
            ->setParameter(1, $id)
            ->orderBy('t.created', 'DESC');

        $response['data'] = [];

        /** @var Training $training */
        foreach ($qb->getQuery()->getResult() as $training) {
            $response['data'][] = $training;
        }

        return new JsonResponse($response);
    }

    /**
     * Return all users
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong."
     *      }
     * )
     *
     * @Get("/users")
     *
     * @QueryParam(name="name", description="Name and/or surname", strict=false)
     * @QueryParam(name="email", description="username or email", strict=false)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        /** @var QueryBuilder $qb */
        $qb = $em->createQueryBuilder();
        $qb->select(
            'u.id',
            'u.username',
            'u.name',
            'u.surname'
        )
            ->from('WayTrainingBundle:User', 'u')
            ->where($qb->expr()->neq('u.id', $user->getId()));

        if(null !== $nameParam = $request->query->get('name')) {
            $name = $qb->expr()->literal('%' . addcslashes($nameParam, '%_') . '%');
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('u.name', $name),
                    $qb->expr()->like('u.surname', $name)
                )
            );
        }

        if(null !== $emailParam = $request->query->get('email')) {
            $email = $qb->expr()->literal('%' . addcslashes($emailParam, '%_') . '%');
                $qb->orWhere($qb->expr()->like('u.username', $email));
        }

        $response['data'] = [];

        /** @var Training $training */
        foreach ($qb->getQuery()->getResult() as $user) {
            $response['data'][] = $user;
        }

        return new JsonResponse($response);
    }

    /**
     * Return training by training id
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong."
     *      }
     * )
     *
     * @Get("/training/{id}")
     *
     * @param int $id
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function getTrainingAction($id)
    {

        if (empty($id)) {
            throw new BadRequestHttpException('bad training id received');
        }

        $em = $this->getDoctrine()->getManager();

        /** @var Training $training */
        $training = $em->getRepository('WayTrainingBundle:Training')->find($id);
        if (null === $training) {
            throw new BadRequestHttpException('training not found');
        }

        $logger = $this->get('logger');
        $response['resume'] = $training->getTrainingResume()->jsonSerialize();
        $logger->info('resume', $training->getTrainingResume()->jsonSerialize());

        $elasticSearchService = $this->get('training.elastic_search_service');

        $response['locations'] = array_values($elasticSearchService->getTrainingData(
            $training->getUser()->getId(),
            $training->getTrainingToken()
        ));

        return new JsonResponse($response);
    }

    /**
     *  Add observers association for one user
     *
     * @ApiDoc(
     *      section="UserManagement",
     *      statusCodes={
     *          204="Returned when successful.",
     *          401="Returned when the user is not authorized."
     *      }
     * )
     *
     * @Put("/user/observer/{id}", requirements = {"id" = "\d+"})
     * @RequestParam(array=true, name="data", description="new observers", strict=true)
     *
     * @param mixed $id
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function addObserverAction($id, ParamFetcher $paramFetcher){

        if(true === empty($data = $paramFetcher->all()['data'])) {
            throw new BadRequestHttpException('no observers to add');
        }

        return new JsonResponse($this->get('observers')->addObserver($id, $data));
    }

    /**
     * Return pending observer request for one user
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong."
     *      }
     * )
     *
     * @Get("/user/{id}/pending/observers")
     *
     * @param int $id
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function getPendingObserversAction($id)
    {
        return new JsonResponse($this->get('observers')->getPendingObservers($id));
    }

    /**
     * Return pending observer request for one user
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong."
     *      }
     * )
     *
     * @Get("/user/pending/confirm/{id}")
     *
     * @param int $id pendind id
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function setPendingObserversAction($id)
    {
        return new JsonResponse($this->get('observers')->setPendingObservers($id), 200);
    }

    /**
     * Delete observers association for one user
     *
     * @ApiDoc(
     *      section="UserManagement",
     *      statusCodes={
     *          204="Returned when successful.",
     *          401="Returned when the user is not authorized."
     *      }
     * )
     *
     * @Delete("/user/observer/{id}", requirements = {"id" = "\d+"})
     * @RequestParam(array=true, name="data", description="new observers", strict=true)
     *
     * @param mixed $id
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return JsonResponse
     */
    public function deleteObserverAction($id, ParamFetcher $paramFetcher){

        if(true === empty($data = $paramFetcher->all()['data'])) {
            throw new BadRequestHttpException('no observers to add');
        }

        return new JsonResponse($this->get('observers')->deleteObserver($id, $data));
    }

    /**
     * Return all observers for one user
     *
     * @ApiDoc(
     *      section="TrainingManagement",
     *      statusCodes={
     *          200="Returned when successful.",
     *          400="Returned when request data are wrong."
     *      }
     * )
     *
     * @Get("/user/{id}/observers")
     *
     * @param $id
     * @return JsonResponse
     */
    public function getObserversAction($id)
    {
        return new JsonResponse($this->get('observers')->getObservers($id));
    }
}