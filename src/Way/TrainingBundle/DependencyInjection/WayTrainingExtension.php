<?php

namespace Way\TrainingBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class WayTrainingExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (!isset($config['auth_token'])) {
            throw new \InvalidArgumentException('The parameter auth_token must be defined');
        }

        if (!isset($config['elastic_search'])) {
            throw new \InvalidArgumentException('The parameter elastic_search must be defined');
        }

        $container->setParameter('way_training.auth_token', $config['auth_token']);
        $container->setParameter('way_training.elastic_search', $config['elastic_search']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

    }
}
