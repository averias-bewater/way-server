<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 10.06.15
 * Time: 04:09
 */

namespace Way\TrainingBundle\Service;


use Elastica\Bulk;
use Elastica\Client;
use Elastica\Document;
use Elastica\Exception\Bulk\ResponseException;
use Elastica\Filter\Bool as ElasticaBoolFilter;
use Elastica\Query;
use Elastica\Query\MatchAll;
use Elastica\Filter\Term;
use Elastica\Query\Filtered;
use Elastica\Result;
use Elastica\ResultSet;
use Elastica\Search;
use Monolog\Logger;

class ElasticSearch {

    /** @var Client  */
    private $elasticaClient;

    /** @var  string */
    private $index;

    /** @var  string */
    private $type;

    /** @var Logger  */
    private $logger;

    /**
     * @param array $elasticSearchConfig
     */
    public function __construct(array $elasticSearchConfig, Logger $logger) {

        $this->index = $elasticSearchConfig['index_prefix'];
        $this->type = $elasticSearchConfig['type'];
        $this->elasticaClient = new Client(
            [
                'host' => $elasticSearchConfig['host'],
                'port' => $elasticSearchConfig['port']
            ]
        );
        $this->logger = $logger;
    }

    /**
     * @param $userId
     * @param $trainingToken
     * @return array
     */
    public function getTrainingData ($userId, $trainingToken) {
        /** @var Search $search */
        $search = $this->getSearch($userId, $trainingToken);

        $trainingData = [];

        /** @var ResultSet $resultSet */
        foreach($search->scanAndScroll() as $resultSet) {

            /** @var Result $result */
            foreach($resultSet->getResults() as $result) {
                $data = $result->getData();
                $trainingData[$data['id']] = $data;
                $this->logger->info('id', ['id' => intval($data['id'])]);
            }
        }

        if (false === empty($trainingData)) {
            ksort($trainingData);
        }

        $this->logger->info("Training Data", $trainingData);
        return $trainingData;
    }

    /**
     * @param $userId
     * @param $trainingToken
     * @return Bulk\Action[]
     */
    public function deleteTrainingData ($userId, $trainingToken) {

        $search = $this->getSearch($userId, $trainingToken);
        $bulk = new Bulk($this->elasticaClient);

        /** @var ResultSet $resultSet */
        foreach($search->scanAndScroll() as $resultSet) {

            /** @var Result $result */
            foreach($resultSet->getResults() as $result) {
                $bulk->addAction(new Bulk\Action\DeleteDocument(
                    new Document($result->getId(), [], $this->type, $this->index . $userId)
                ));
            }
        }

        try {
            $deletions = count($bulk->getActions());
            if($deletions > 0) {
                $bulk->send();
            }
        } catch(ResponseException $e) {
            $this->logger->info("ES delete locations exception", $e->getFailures());
            throw new \RuntimeException(json_encode($e->getFailures()));
        }


        $this->logger->info("Deleted Locations: " . $deletions);
        return $deletions;
    }

    /**
     * @param $userId
     * @param $trainingToken
     * @param $clientLocations
     * @return Bulk\Action[]
     */
    public function syncTrainingData ($userId, $trainingToken, $clientLocations) {
        $trainingData = $this->getTrainingData($userId, $trainingToken);
        $this->logger->info("Client locations", $clientLocations);

        $bulk = new Bulk($this->elasticaClient);

        foreach($clientLocations as $location) {
            $this->logger->info("location#" . $location['_id'], $location);
            if (false === isset($trainingData[$location['_id']])) {
                $location['token'] = $trainingToken;
                $location['user_id'] = $userId;
                $location['id'] = $location['_id'];
                unset($location['_id']);

                $bulk->addAction(new Bulk\Action\CreateDocument(
                    new Document('', $location, $this->type, $this->index . $userId)
                ));
            }
        }

        try {
            $creations = count($bulk->getActions());
            if($creations > 0) {
                $bulk->send();
            }
        } catch(ResponseException $e) {
            $this->logger->info("ES sync locations clients exception", $e->getFailures());
            throw new \RuntimeException(json_encode($e->getFailures()));
        }

        return $creations;
    }

    /**
     * @param int $userId
     * @param string $trainingToken
     * @return Search
     */
    private function getSearch ($userId, $trainingToken) {
        $bool = new ElasticaBoolFilter();
        $bool->addMust(new Term(['token' => $trainingToken]));

        $query = new Query(new Filtered(new MatchAll(), $bool));

        $search = new Search($this->elasticaClient);
        $search->addIndex($this->index . $userId);
        $search->addType($this->type);
        $search->setQuery($query);

        return $search;
    }

}