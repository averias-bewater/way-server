<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 06.06.15
 * Time: 03:43
 */

namespace Way\TrainingBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use Way\TrainingBundle\Entity\AuthToken;
use Way\TrainingBundle\Entity\User;

class UserToken {

    /**
     * @var \Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var int
     */
    private $sessionLifetime;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param array $sessionOptions
     * @param \Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(
        EntityManager $em,
        array $sessionOptions,
        TokenGeneratorInterface $tokenGenerator = null
    ) {
        $this->tokenGenerator = $tokenGenerator ?: new UriSafeTokenGenerator();
        $this->em = $em;
        $this->sessionLifetime = isset($sessionOptions['cookie_lifetime']) ? (int) $sessionOptions['cookie_lifetime'] : 0;
    }

    public function save(User $user) {
        $now = new \DateTime();

        // create new AuthToken entity
        $authToken = new AuthToken();
        $authToken->setUser($user);
        $authToken->setToken($this->tokenGenerator->generateToken());
        $authToken->setExpire($now->add(new \DateInterval('PT' . $this->sessionLifetime . 'S')));

        $this->em->persist($authToken);
        $this->em->flush();

        return $authToken;
    }

    public function checkToken($token, $userId) {

        /** @var User $user */
        $user = $this->em->getRepository('WayTrainingBundle:User')->find($userId);

        if (null === $user) {
            throw new AccessDeniedHttpException('user not found');
        }

        /** @var AuthToken $authToken */
        $authToken = $this->em->getRepository('WayTrainingBundle:AuthToken')->findOneBy(
            [
                'token' => $token,
                'user' => $user
            ]
        );

        if (null === $authToken) {
            throw new AccessDeniedHttpException('token not found');
        }

        if($authToken->getExpire() < (new \DateTime())) {
            throw new AccessDeniedHttpException('token expired');
        }

        return $authToken;
    }
}