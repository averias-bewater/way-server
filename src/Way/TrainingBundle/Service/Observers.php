<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 08.08.15
 * Time: 22:40
 */

namespace Way\TrainingBundle\Service;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Way\TrainingBundle\Entity\User;
use Way\TrainingBundle\Entity\Observer;

class Observers {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * @param int $id
     * @param array $data
     * @return array
     */
    public function addObserver($id, $data) {

        /** @var User $user */
        $user = $this->em->getRepository('WayTrainingBundle:User')->find($id);

        if (null === $user) {
            throw new BadRequestHttpException('user does not exist');
        }

        /** @var QueryBuilder $qb */
        $qb = $this->em->createQueryBuilder();
        $qb->select('u')
            ->from('WayTrainingBundle:User', 'u')
            ->where($qb->expr()->in('u.id', $data));

        $userAsObservers = $qb->getQuery()->getResult();

        if(empty($userAsObservers)){
            throw new BadRequestHttpException('no new observers found');
        }

        $alreadyObservers = [];
        $newObservers = [];

        /** @var User $userAsObserver */
        foreach ($userAsObservers as $userAsObserver) {
            if (true === $userAsObserver->isObserverOf($user->getId())) {
                $alreadyObservers[] = $userAsObserver->getId();
                continue;
            }

            if($user->getId() !== $userAsObserver->getId()) {
                /** @var Observer $observer */
                $observer = new Observer();
                $observer->setUser($user);
                $observer->setObserver($userAsObserver);
                $this->em->persist($observer);

                $newObservers[] = $userAsObserver->getId();
            }

        }

        $this->em->flush();

        return ['already_observers' => $alreadyObservers, 'new_observers' => $newObservers];
    }

    /**
     * @param int $id
     * @param array $data
     * @return array
     */
    public function deleteObserver($id, $data) {

        /** @var User $user */
        $user = $this->em->getRepository('WayTrainingBundle:User')->find($id);

        if (null === $user) {
            throw new BadRequestHttpException('user does not exist');
        }

        /** @var QueryBuilder $qb */
        $qb = $this->em->createQueryBuilder();
        $qb->select('o')
            ->from('WayTrainingBundle:Observer', 'o')
            ->innerJoin('o.user', 'u')
            ->innerJoin('o.observer', 'ob')
            ->where($qb->expr()->in('ob.id', $data))
            ->andWhere($qb->expr()->eq('u.id', $user->getId()));

        $observers = $qb->getQuery()->getResult();

        $deletedObservers = [];

        /** @var Observer $observer */
        foreach ($observers as $observer) {
            $this->em->remove($observer);
            $deletedObservers[] = $observer->getObserver()->getId();
        }

        if(false === empty($deletedObservers)) {
            /** @var QueryBuilder $qb */
            $qb = $this->em->createQueryBuilder();
            $qb->select('o')
                ->from('WayTrainingBundle:Observer', 'o')
                ->innerJoin('o.user', 'u')
                ->innerJoin('o.observer', 'ob')
                ->where($qb->expr()->in('u.id', $deletedObservers))
                ->andWhere($qb->expr()->eq('ob.id', $user->getId()));

            $observers = $qb->getQuery()->getResult();

            /** @var Observer $observer */
            foreach ($observers as $observer) {
                $this->em->remove($observer);
            }
        }
        $this->em->flush();

        return ['deleted_observers' => $deletedObservers];
    }

    /**
     * @param int $id
     * @return array
     */
    public function getPendingObservers($id) {

        /** @var User $user */
        $user = $this->em->getRepository('WayTrainingBundle:User')->find($id);

        if (null === $user) {
            throw new BadRequestHttpException('user does not exist');
        }

        $pendingsRequest = $this->em->getRepository('WayTrainingBundle:Observer')->findBy(['observer' => $user, 'status' => 'pending']);
        $response = [];

        if(null === $pendingsRequest) {
            return $response;
        }

        /** @var Observer $pendingRequest */
        foreach ($pendingsRequest as $pendingRequest) {
            $response[] = [
                'id' => $pendingRequest->getId(),
                'requestor_name' => $pendingRequest->getUser()->getName(),
                'requestor_surname' => $pendingRequest->getUser()->getSurname(),
                'requestor_username' => $pendingRequest->getUser()->getUsername()
            ];
        }

        return $response;
    }

    /**
     * @param int $id
     * @return array
     */
    public function setPendingObservers($id) {

        /** @var User $user */
        $pendingRequest = $this->em->getRepository('WayTrainingBundle:Observer')->find($id);

        if (null === $pendingRequest) {
            throw new BadRequestHttpException('pending observer request does not exist');
        }

        if ('pending' !== $pendingRequest->getStatus()) {
            throw new BadRequestHttpException('pending observer request is not pending');
        }

        $pendingRequest->setStatus('active');

        $inversePending = $this->em
            ->getRepository('WayTrainingBundle:Observer')
            ->findOneBy(['user' => $pendingRequest->getObserver(), 'observer' => $pendingRequest->getUser()]);

        if(null === $inversePending) {
            $inversePending = new Observer();
            $inversePending->setObserver($pendingRequest->getUser());
            $inversePending->setUser($pendingRequest->getObserver());
            $inversePending->setStatus('active');

            $this->em->persist($inversePending);
        } else {
            $inversePending->setStatus('active');
        }

        $this->em->flush();

        return [];
    }

    /**
     * Return active observer for one User
     *
     * @param int $id
     * @return array
     */
    public function getObservers($id) {

        /** @var User $user */
        $user = $this->em->getRepository('WayTrainingBundle:User')->find($id);

        if (null === $user) {
            throw new BadRequestHttpException('user does not exist');
        }

        $response = [];

        /** @var Observer $observer */
        foreach ($user->getActiveObservers() as $observer) {
            $response[] = [
                'id' => $observer->getObserver()->getId(),
                'name' => $observer->getObserver()->getName(),
                'surname' => $observer->getObserver()->getSurname(),
                'username' => $observer->getObserver()->getUsername()
            ];
        }

        return $response;
    }
}