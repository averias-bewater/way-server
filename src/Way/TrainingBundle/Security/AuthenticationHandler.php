<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 05.06.15
 * Time: 00:52
 */
namespace Way\TrainingBundle\Security;

use Way\TrainingBundle\Entity\AuthToken;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Way\TrainingBundle\Service\UserToken;

class AuthenticationHandler implements AuthenticationFailureHandlerInterface, AuthenticationSuccessHandlerInterface {

    const SESSION_KEY = 'auth_token';

    /**
     * @var array
     */
    private $authTokenConfig;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \JMS\Serializer\Serializer
     */
    private $serializer;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;

    /**
     * @var int
     */
    private $sessionLifetime;

    /**
     * @var UserToken
     */
    private $authTokenService;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param \JMS\Serializer\Serializer $serializer
     * @param \Symfony\Component\HttpFoundation\Session\Session $session
     * @param array $authTokenConfig
     * @param array $sessionOptions
     * @param UserToken $authTokenService
     */
    public function __construct(
        EntityManager $em,
        Serializer $serializer,
        Session $session,
        array $authTokenConfig,
        array $sessionOptions,
        UserToken $authTokenService
    ) {
        $this->authTokenConfig = $authTokenConfig;
        $this->em = $em;
        $this->serializer = $serializer;
        $this->session = $session;
        $this->sessionLifetime = isset($sessionOptions['cookie_lifetime']) ? (int) $sessionOptions['cookie_lifetime'] : 0;
        $this->authTokenService = $authTokenService;
    }

    /**
     * This is called when an interactive authentication attempt fails. This is
     * called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Symfony\Component\Security\Core\Exception\AuthenticationException $exception
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        throw new BadRequestHttpException('wrong username or password');
    }

    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
        // delete existing token
        if($this->session->has(self::SESSION_KEY)) {
            $authToken = $this->em->getRepository('WayTrainingBundle:AuthToken')->findOneBy(array(
                'token' => $this->session->get(self::SESSION_KEY)
            ));
            if($authToken !== null) {
                $this->em->remove($authToken);
            }
        }

        /** @var AuthToken $authToken */
        $authToken = $this->authTokenService->save($token->getUser());
        // store token in session
        $this->session->set(self::SESSION_KEY, $authToken->getToken());

        return new JsonResponse(array(
            'token' => $authToken->getToken(),
            'expire' => $authToken->getExpire()->format('c')
        ));
    }

}