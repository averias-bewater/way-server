<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 05.06.15
 * Time: 01:26
 */
namespace Way\TrainingBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutHandler implements LogoutSuccessHandlerInterface {

    /**
     * Creates a Response object to send upon a successful logout.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function onLogoutSuccess(Request $request) {
        return new JsonResponse(['message' => 'successfully logged out']);
    }
}