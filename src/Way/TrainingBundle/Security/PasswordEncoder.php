<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 05.06.15
 * Time: 21:35
 */

namespace Way\TrainingBundle\Security;


use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class PasswordEncoder implements PasswordEncoderInterface {

    /**
     * @inheritdoc
     */
    public function encodePassword($raw, $salt)
    {
        return md5($raw.$salt);
    }

    /**
     * @inheritdoc
     */
    public function isPasswordValid ($encoded, $raw, $salt) {
        return $encoded === $this->encodePassword($raw, $salt);
    }

}