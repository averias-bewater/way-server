<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 06.06.15
 * Time: 00:32
 */

namespace Way\TrainingBundle\Listener;

use Monolog\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ExceptionListener {
    /**
     * @var string
     */
    private $environment;

    /**
     * @var \Monolog\Logger
     */
    private $logger;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * Constructor
     *
     * @param \AppKernel $kernel
     * @param \Monolog\Logger $logger
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface
     */
    public function __construct(
        \AppKernel $kernel,
        Logger $logger,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker
    )
    {
        $this->environment = $kernel->getEnvironment();
        $this->logger = $logger;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Returns the exception as JSON
     * In prod-environment: only display a general error message
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        // handle 401, 403
        if($exception instanceof AccessDeniedException) {
            if(null === $this->$tokenStorage->getToken() || false === $this->authorizationChecker->isGranted('ROLE_USER')) {
                $exception = new UnauthorizedHttpException('NOT STATELESS! @see /user/login', 'unauthorized');
            } else {
                $exception = new AccessDeniedHttpException('access denied! no permissions');
            }
        }

        $isHttpException = $exception instanceof HttpExceptionInterface;

        $serialize = array(
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile() . ' in line ' . $exception->getLine(),
            'trace' => $exception->getTraceAsString()
        );

        if(in_array($this->environment, ['dev', 'test'])) {
            // dev|test exception
            $body = $serialize;
        } else {
            // prod exception
            $body = array(
                'code' => $isHttpException ? $exception->getStatusCode() : $serialize['code'],
                'message' => 'an error occurred while processing your request'
            );

            // log the exception
            $this->logger->addError(json_encode($serialize));
        }

        $response = new JsonResponse($body, 500);

        // set http status code and headers on http error
        if($isHttpException) {
            $response->setStatusCode($exception->getStatusCode());
            foreach($exception->getHeaders() as $header => $value) {
                $response->headers->set($header, $value);
            }
        }

        $event->setResponse($response);
    }
}