<?php
/**
 * Created by PhpStorm.
 * User: bewater
 * Date: 05.06.15
 * Time: 22:59
 */

namespace Way\TrainingBundle\Listener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class AuthTokenListener {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var array
     */
    private $authTokenConfig;

    /**
     * @var int
     */
    private $sessionLifetime;

    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
     */
    private $tokenStorage;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param array $authTokenConfig
     * @param array $sessionOptions
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManager $em,
        array $authTokenConfig,
        array $sessionOptions,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->authTokenConfig = $authTokenConfig;
        $this->sessionLifetime = isset($sessionOptions['cookie_lifetime']) ? (int) $sessionOptions['cookie_lifetime'] : 0;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Validates WebToken
     *
     * @param GetResponseEvent $event
     *
     * @throws \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function onKernelRequest(GetResponseEvent $event) {
        $request = $event->getRequest();
        $requestUri =  $request->getRequestUri();

        //exclude URI
        foreach (['/way/user/save', '/training'] as $uri) {
            if(false !== strpos($requestUri, $uri)) {
                return;
            }
        }

        // skip sub requests
        if(HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $authHeader = $request->headers->get($this->authTokenConfig['header']);
        if(null === $authHeader) {
            throw new AccessDeniedHttpException('no ' . $this->authTokenConfig['header'] . ' header found');
        }

        $user = $this->tokenStorage->getToken()->getUser();

        $authToken = $this->em->getRepository('WayTrainingBundle:AuthToken')->findOneBy(array(
            'token' => $authHeader,
            'user' => $user
        ));

        if(null === $authToken) {
            throw new UnauthorizedHttpException('token/user combination not found');
        }

        $now = new \DateTime();
        if($authToken->getExpire() < $now) {
            throw new UnauthorizedHttpException('no challenge', 'token expired');
        }

        // update expire
        $now->add(new \DateInterval('PT' . $this->sessionLifetime . 'S'));
        $authToken->setExpire($now);

        $this->em->flush();
        return;
    }

}