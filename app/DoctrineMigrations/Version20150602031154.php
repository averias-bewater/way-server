<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150602031154 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE User (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, salt VARCHAR(255) NOT NULL, register_date DATETIME NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, height INT NOT NULL, weight DOUBLE PRECISION NOT NULL, birth_date DATETIME NOT NULL, gender VARCHAR(1) NOT NULL, UNIQUE INDEX username_unique (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE AuthToken (token VARCHAR(255) NOT NULL, user_id INT NOT NULL, created DATETIME NOT NULL, expire DATETIME NOT NULL, INDEX IDX_6F049C33A76ED395 (user_id), PRIMARY KEY(token)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE AuthToken ADD CONSTRAINT FK_6F049C33A76ED395 FOREIGN KEY (user_id) REFERENCES User (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE AuthToken DROP FOREIGN KEY FK_6F049C33A76ED395');
        $this->addSql('DROP TABLE User');
        $this->addSql('DROP TABLE AuthToken');
    }
}
